# DiningCity 小程序路径大全

### 1 首页 Index page

`Path` pages/index

- 不接受任何参数
- 不区分城市，根据地理位置定位

### 2 优惠合集 Deal collection

`Path` deals/list?gid=1537

- `gid` 优惠合集 ID（必填），可从 DiningCity 官网对应的优惠合集页面的地址栏中找到该 ID
- 不区分城市
- 暂不接受 accessCode 参数
- 暂不接受 source 参数

### 3 单个优惠 Single deal

`Path` deals/detail?deal_id=5607

- `deal_id` 优惠 ID（必填），可从 DiningCity 官网对应的优惠页面的地址栏中找到该 ID
- 不区分城市
- 暂不接受 accessCode 参数
- 暂不接受 source 参数

### 4 餐厅合集 Restaurant collection

`Path` collections/list?gid=1521

- `gid` 餐厅合集 ID（必填），可从 DiningCity 官网对应的餐厅合集页面的地址栏中找到该 ID
- 不区分城市
- 暂不接受 accessCode 参数
- 暂不接受 source 参数

### 5 单个餐厅页面（普通预定）Single restaurant (Normal booking)

`Path` collections/detail?rid=2052249

- `rid` 单个餐厅 ID（必填），可从 DiningCity 官网对应的餐厅页面的地址栏中找到该 ID
- 不区分城市
- 暂不接受 accessCode 参数
- 暂不接受 source 参数

### 6 活动首页 Event home page

`Path` events/detail?project=rwcn_summer_2019_gba_sz&accessCode=xxx&source=xxx

- `project` 活动 ID（必填）
- `accessCode` 自定义访问码（选填）
- `source` 自定义来源（选填）
- 不区分城市，根据地理位置定位

### 7 活动餐厅列表 Event restaurant list

`Path` events/restaurants?project=rwcn_summer_2019_gba_sz&accessCode=xxx&source=xxx

- `project` 活动 ID（必填）
- `accessCode` 自定义访问码（选填）
- `source` 自定义来源（选填）
- 不区分城市，根据地理位置定位

### 8 活动单个餐厅 Event single restaurant

`Path` events/restaurant?project=rwcn_summer_2019_gba_sz&rid=2051647&accessCode=xxx&source=xxx

- `project` 活动 ID（必填）
- `rid` 餐厅 ID
- `accessCode` 自定义访问码（选填）
- `source` 自定义来源（选填）
- 不区分城市，根据地理位置定位

### 9 活动餐厅合集 Event restaurant collection

`Path` events/collection?project=rwcn_summer_2019_gba_sz&cid=257&accessCode=xxx&source=xxx

- `project` 活动 ID（必填）
- `cid` 合集 ID
- `accessCode` 自定义访问码（选填）
- `source` 自定义来源（选填）
- 不区分城市，根据地理位置定位
